<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlists;

class PlaylistController extends Controller
{
    public function displaySongs(){
        return DB::table('playlists')->get();
    }


    public function store(Request $request){

        $newPlaylists = new Playlists();
        $newPlaylists->name = $request->title;
        $newPlaylists>save();
        return $newPlaylists;
}
}

