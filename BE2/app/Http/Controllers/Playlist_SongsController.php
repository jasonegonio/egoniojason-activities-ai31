<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlists_Songs;

class Playlist_SongsController extends Controller
{
    public function displaySongs(){
        return DB::table('playlists_songs')->get();
    }

    public function store(Request $request){

        $newPlaylists_Songs = new Playlists_Songs();
        $newPlaylists_Songs->song_id = $request->song_id;
        $newPlaylists_Songs->playlist_id = $request->playlist_id;
        $newPlaylists_Songssave();
        return $newPlaylists_Songs;
}
}
